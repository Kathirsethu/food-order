package com.foodorder.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.foodorder.R
import com.foodorder.model.Order

class OrdersAdapter(private val orderList: MutableList<Order>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val TYPE_HEADER = 1
        const val TYPE_ORDER_ITEM = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_HEADER ->
                ViewHolderHeader(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_order_date, parent, false)
                )
            TYPE_ORDER_ITEM ->
                ViewHolderOrderItem(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_order_name, parent, false)
                )
            else ->
                ViewHolderOrderItem(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_order_name, parent, false)
                )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolderHeader)
            holder.tvOrderDate.text = orderList[position].date
        else if (holder is ViewHolderOrderItem)
            holder.tvOrderName.text = orderList[position].name
    }

    override fun getItemCount(): Int {
        return orderList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (orderList[position].isHeader)
            TYPE_HEADER
        else
            TYPE_ORDER_ITEM
    }

    open class ViewHolderOrderItem(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvOrderName: TextView = itemView.findViewById(R.id.tvOrderName)
    }

    open class ViewHolderHeader(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvOrderDate: TextView = itemView.findViewById(R.id.tvOrderDate)
    }
}
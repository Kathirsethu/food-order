package com.foodorder.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class Order(
    @SerializedName("order_id")
    val orderId: Int,
    val name: String,
    @SerializedName("order_date")
    val orderDate: String,
    var isHeader: Boolean
) : Parcelable {

    @IgnoredOnParcel
    private val originalDateFormatter = SimpleDateFormat("dd-MM-yyyy hh:mm aa", Locale.getDefault())
    @IgnoredOnParcel
    private val listDateFormatter = SimpleDateFormat("dd - MMM yyyy", Locale.getDefault())
    @IgnoredOnParcel
    var date: String = listDateFormatter.format(originalDateFormatter.parse(orderDate)!!)
}